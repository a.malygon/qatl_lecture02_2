import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

//Checking of Admin Panel's main menu

public class Main {

    public static void main(String[] args) {
        WebDriver driver = initChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        loginToAdminPanel(driver);

        //get Main Menu's elements
        List<WebElement> elements = driver.findElements(By.className("maintab"));
        List<String> elementsText = new ArrayList<String>();

        for (WebElement element: elements) {
            elementsText.add(element.getText());
        }

        //print page title for all 1-st level elements from Main Menu
        //check titles matching after page refreshing
        for (String text: elementsText) {
            WebElement element = driver.findElement(By.linkText(text));
            element.click();
            String currentUrl = driver.getCurrentUrl();
            System.out.println("header: " + driver.getTitle());

            try {
                driver.navigate().refresh();
                if (!driver.getCurrentUrl().equals(currentUrl))
                    throw new Exception("links don't match after page refreshing");
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
        }

        driver.quit();
    }

    private static void logoutFromAdminPanel(WebDriver driver) {
        WebElement userIcon = driver.findElement(By.id("header_employee_box"));
        userIcon.click();

        WebElement logout = driver.findElement(By.id("header_logout"));
        logout.click();
    }

    private static void loginToAdminPanel(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0");

        WebElement loginField = driver.findElement(By.id("email"));
        loginField.sendKeys("webinar.test@gmail.com");

        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebElement submitLoginBtn = driver.findElement(By.name("submitLogin"));
        submitLoginBtn.click();
    }

    private static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        return new ChromeDriver(options);
    }
}